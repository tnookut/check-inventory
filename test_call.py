from twilio.rest import Client
import json

secret = json.load(open('secret.json'))

account_sid = secret['TWILIO_ACCOUNT_SID']
auth_token = secret['TWILIO_AUTH_TOKEN']
sender = secret['TWILIO_NUMBER']
receiver = secret['PHONE_NUMBER']

client = Client(account_sid, auth_token)

call = client.calls.create(
                        url='http://demo.twilio.com/docs/voice.xml',
                        to=receiver,
                        from_=sender
                    )

print(call.sid)
