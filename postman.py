import json
import ast
import SMS
import call
import webbrowser
from playsound import playsound
import os
import time
import http.client

url = "https://www.ti.com/store/ti/en/p/product/?p=TPS82140SILT&keyMatch=TPS82140&tisearch=search-everything&usecase=GPN"
conn = http.client.HTTPSConnection("www.ti.com")
payload = json.dumps(["TPS82140SILT"])
headers = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
  'Content-Type': 'application/json'
}

def stock_status():
    conn.request("POST", "/productmodel/opn/stockforecast?locale=en-US", payload, headers)
    res = conn.getresponse()
    data = res.read()
    print(res.code)

    if res.code == 200:
        obj = ast.literal_eval(data.decode("utf-8"))
        return obj["TPS82140SILT"]["currentInventory"]
    else:
        return "Error"

def notify(quantity):
    if quantity == "0":
        print("Out of stock")
    elif quantity == "Error":
        print(quantity)
        SMS.sms()
        webbrowser.open(url)
    else:
        print("PRODUCT IN STOCK. PLACE ORDER ASAP!!!")
        call.call()
        SMS.sms()
        webbrowser.open(url)
        while(True):
            playsound(os.getcwd() + '/Super Mario Bros. medley.mp3')

while(True):
    quantity = stock_status()
    notify(quantity)
    time.sleep(30)

