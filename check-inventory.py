import webbrowser
import requests
from bs4 import BeautifulSoup
from playsound import playsound
import os
import time

def get_page_html(url):
    headers = {"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36"}
    page = requests.get(url, headers=headers)
    return page.content


def check_item_in_stock(page_html):
    soup = BeautifulSoup(page_html, 'html.parser')
    out_of_stock_divs = soup.find("div", {"class": "product-status"})

    try:
        status = out_of_stock_divs.text
    except AttributeError:
        status = 'In stock'
    print(status)
    return status

def check_inventory():
    url = 'https://www.ti.com/store/ti/en/p/product/?p=TPS82140SILT&keyMatch=TPS82140&tisearch=search-everything&usecase=GPN'
    page_html = get_page_html(url)
    status = check_item_in_stock(page_html)
    if status == 'Out of stock':
        pass
    else:
        webbrowser.open(url)
        while(True):
            playsound(os.getcwd() + '/Super Mario Bros. medley.mp3')

while(True):
    check_inventory()
    time.sleep(60)