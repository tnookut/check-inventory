import http.client
import json

conn = http.client.HTTPSConnection("www.ti.com")
payload = json.dumps(["TPS82140SILT"])
headers = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36',
  'Content-Type': 'application/json'
}

while(True):
  conn.request("POST", "/productmodel/opn/stockforecast?locale=en-US", payload, headers)
  res = conn.getresponse()
  data = res.read()
  print(res.code)
  print(data.decode("utf-8"))
