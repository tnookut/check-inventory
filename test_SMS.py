from twilio.rest import Client
import json

secret = json.load(open('secret.json'))

account_sid = secret['TWILIO_ACCOUNT_SID']
auth_token = secret['TWILIO_AUTH_TOKEN']
client = Client(account_sid, auth_token)
sender = secret['TWILIO_NUMBER']
receiver = secret['PHONE_NUMBER']

url = 'https://www.ti.com/store/ti/en/p/product/?p=TPS82140SILT&keyMatch=TPS82140&tisearch=search-everything&usecase=GPN'

message = client.messages.create(
                              body=url,
                              to=receiver,
                              from_=sender
                          )

print(message.sid)
