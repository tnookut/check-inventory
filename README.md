check-inventory.py checks the TPS82140SILT stock status every 1 minute.

The script prints out stock status in terminal. 

When the product is in stock, it will open the product URL in the browser and play Mario Kart theme song so keep your sound on. 🔊

Product URL:
https://www.ti.com/store/ti/en/p/product/?p=TPS82140SILT&keyMatch=TPS82140&tisearch=search-everything&usecase=GPN


Setting up:

Create and activate virtual environment.

```
python -m venv env
source env/Script/activate
```

Install dependencies.

```
pip install -r requirements.txt
```

To run the script:

```
python check-inventory.py
```


